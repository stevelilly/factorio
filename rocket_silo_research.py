#!/usr/bin/env python3

from research import transitive_research

# We assume these were researched manually in the bootstrap phase
built = set(['Automation', 'Logistics'])

# We'll need this at some point, doing it now gives as faster machines
transitive_research('Automation 2', built)

# Start building these early - can build them slower
transitive_research('Rocket control unit', built)

transitive_research('Rocket fuel', built)
transitive_research('Rocket silo', built)
