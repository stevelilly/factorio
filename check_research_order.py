#!/usr/bin/env python3

import sys

from research import research

researched = set()

for techName in sys.stdin:
    techName = techName.rstrip()
    if techName not in research:
        print("Unknown tech %s" % techName)
        sys.exit(1)
    for dep in research[techName]['depends']:
        if dep not in researched:
            print("Cannot research %s before %s" % (techName, dep))
            sys.exit(1)
    researched.add(techName)

print("Research order is OK")
