import yaml     # pip install pyyaml

def add_empty_key(data, key):
    if key not in data:
        data[key] = []

with open("research.yml", mode="rb") as file:
    research = yaml.safe_load(file)['research']
    for tech in research.values():
        add_empty_key(tech, 'depends')
        add_empty_key(tech, 'effects')

print("Loaded %d research items" % len(research))

def check_dependencies():
    techs = research.keys()
    missing = set()
    for tech in techs:
        for dep in research[tech]['depends']:
            if dep not in techs:
                missing.add(dep)
    if missing:
        print("These dependencies are missing:")
        missing = list(missing)
        missing.sort()
        for tech in missing:
            print(tech)

check_dependencies()

def transitive_research(tech, seen):
    if tech in seen:
        return
    for dep in research[tech]['depends']:
        transitive_research(dep, seen)
    print_tech(tech)
    seen.add(tech)

def print_tech(techName):
    science = research[techName]['science']
    print("%s\t%s\t%d\t%d" % (techName, science['type'], science['amount'], science['time']))
