#!`/bin/env python`

from recipes import recipes
from module_configurations import module_configurations

pollution_data = {}

def find_all_pollution_data():
	while True:
		# find a recipe we don't have data for yet
		product_to_find = None
		for product in recipes.keys():
			if product not in pollution_data:
				product_to_find = product
				break
		if product_to_find == None:
			break
		pollution_data[product_to_find] = calculate_pollution_data(product_to_find)


def has_productivity_module(configuration):
	return "Prod" in configuration['name']


def calculate_pollution_data(product):
	recipe = recipes[product]
	ingredient_pollution = 0
	for ingredient, qty in recipe['ingredients'].items():
		ingredient_pollution += lookup_pollution_data(ingredient) * qty
	if 'machine' in recipe:
		machine_type = recipe['machine']
	else:
		machine_type = 'assembler'
	def pollution_for_configuration(configuration):
		crafting_time = recipe['time'] / configuration['speed']
		if 'output' in recipe:
			output = recipe['output']
		else:
			output = 1
		output *= configuration['productivity']
		total_pollution = ingredient_pollution + configuration['pollution'] * crafting_time
		return total_pollution / output
	if 'prod_module' in recipe:
		productivity_module_allowed = recipe['prod_module']
	else:
		productivity_module_allowed = True
	configurations = module_configurations[machine_type]
	if not productivity_module_allowed:
		configurations = filter(lambda x : not has_productivity_module(x), configurations)
	best_configuration, best_pollution = argmin(pollution_for_configuration, configurations)
	print("Pollution for %s is %f per item with %s" % (product, best_pollution, best_configuration['name']))
	return best_pollution


def argmin(f, choices):
	best_choice = None
	best_value = None
	for choice in choices:
		value = f(choice)
		if best_choice is None or value < best_value:
			best_choice = choice
			best_value = value
	return best_choice, best_value


def lookup_pollution_data(product):
	if product in pollution_data:
		return pollution_data[product]
	else:
		pollution = calculate_pollution_data(product)
		pollution_data[product] = pollution
		return pollution


# TODO tiebreak based on the cost of the modules themselves, e.g. 3xEff1 is much cheaper than 2xEff2
find_all_pollution_data()
