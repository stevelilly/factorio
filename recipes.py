recipes = {
	'Water': {
		'time': 10,
		'machine': 'pump',
		'ingredients': {}
	},

	'Iron Ore': {
		'time': 1,
		'machine': 'miner',
		'ingredients': {}
	},

	'Copper Ore': {
		'time': 1,
		'machine': 'miner',
		'ingredients': {}
	},

	'Stone': {
		'time': 1,
		'machine': 'miner',
		'ingredients': {}
	},

	'Coal': {
		'time': 1,
		'machine': 'miner',
		'ingredients': {}
	},

	'Crude Oil': {
		'time': 1,
		'output': 10,	# assuming yield is 100%
		'machine': 'pumpjack',
		'ingredients': {}
	},

	'Iron Plate': {
		'time': 3.2,
		'ingredients': {
			'Iron Ore': 1
		},
		'machine': 'furnace'
	},

	'Copper Plate': {
		'time': 3.2,
		'ingredients': {
			'Copper Ore': 1
		},
		'machine': 'furnace'
	},
	
	'Logistic Science Pack': {
		'time': 6,
		'ingredients': {
			'Transport Belt': 1,
			'Inserter': 1
		}
	},

	'Transport Belt': {
		'time': 0.5,
		'output': 2,
		'ingredients': {
			'Iron Plate': 1,
			'Iron Gear Wheel': 1
		},
		'prod_module': False
	},

	'Inserter': {
		'time': 0.5,
		'ingredients': {
			'Iron Plate': 1,
			'Iron Gear Wheel': 1,
			'Electronic Circuit': 1
		},
		'prod_module': False
	},

	'Iron Gear Wheel': {
		'time': 0.5,
		'ingredients': {
			'Iron Plate': 2
		}
	},

	'Electronic Circuit': {
		'time': 0.5,
		'ingredients': {
			'Iron Plate': 1,
			'Copper Cable': 3
		}
	},

	'Copper Cable': {
		'time': 0.5,
		'output': 2,
		'ingredients': {
			'Copper Plate': 1
		}
	},

	'Advanced Circuit': {
		'time': 6,
		'ingredients': {
			'Plastic Bar': 2,
			'Copper Cable': 4,
			'Electronic Circuit': 2
		}
	},

	'Plastic Bar': {
		'time': 1,
		'output': 2,
		'machine': 'chemplant',
		'ingredients': {
			'Coal': 1,
			'Petroleum Gas (Basic)': 20
		}
	},

	'Petroleum Gas (Basic)': {
		'time': 5,
		'output': 45,
		'machine': 'refinery',
		'ingredients': {
			'Crude Oil': 100
		}
	},

	'Processing Unit': {
		'time': 10,
		'ingredients': {
			'Electronic Circuit': 20,
			'Advanced Circuit': 2,
			'Sulfuric Acid': 5
		}
	},

	'Sulfuric Acid': {
		'time': 1,
		'output': 50,
		'machine': 'chemplant',
		'ingredients': {
			'Iron Plate': 1,
			'Sulfur': 5,
			'Water': 100
		}
	},

	'Sulfur': {
		'time': 1,
		'output': 2,
		'machine': 'chemplant',
		'ingredients': {
			'Petroleum Gas (Basic)': 30,
			'Water': 30
		}
	},

	'Firearm Magazine': {
		'time': 1,
		'ingredients': {
			'Iron Plate': 4
		}
	},

	'Piercing Rounds Magazine': {
		'time': 3,
		'ingredients': {
			'Copper Plate': 5,
			'Firearm Magazine': 1,
			'Steel Plate': 1
		}
	},

	'Steel Plate': {
		'time': 16,
		'ingredients': {
			'Iron Plate': 5
		},
		'machine': 'furnace'
	},

	'Utility Science Pack': {
		'time': 21,
		'output': 3,
		'ingredients': {
			'Processing Unit': 2,
			'Flying Robot Frame': 1,
			'Low Density Structure': 3
		}
	},

	'Flying Robot Frame': {
		'time': 20,
		'ingredients': {
			'Steel Plate': 1,
			'Battery': 2,
			'Electronic Circuit': 3,
			'Electric Engine Unit': 1
		}
	},

	'Battery': {
		'time': 4,
		'ingredients': {
			'Iron Plate': 1,
			'Copper Plate': 1,
			'Sulfuric Acid': 20
		},
		'machine': 'chemplant'
	},

	'Electric Engine Unit': {
		'time': 10,
		'ingredients': {
			'Electronic Circuit': 2,
			'Engine Unit': 1,
			'Lubricant': 15
		}
	},

	'Engine Unit': {
		'time': 10,
		'ingredients': {
			'Steel Plate': 1,
			'Iron Gear Wheel': 1,
			'Pipe': 2
		}
	},

	'Pipe': {
		'time': 0.5,
		'ingredients': {
			'Iron Plate': 1
		},
		'prod_module': False
	},

	'Lubricant': {
		'time': 1,
		'output': 10,
		'ingredients': {
			'Heavy Oil (Naive)': 10
		},
		'machine': 'chemplant'
	},

	# this would need to consider that the process also yields light oil and petroleum gas, and admeasure the pollution accordingly
	'Heavy Oil (Naive)': {
		'time': 5,
		'output': 25,
		'ingredients': {
			'Crude Oil': 100,
			'Water': 50
		},
		'machine': 'refinery'
	},

	'Light Oil (Naive)': {
		'time': 5,
		'output': 45,
		'ingredients': {
			'Crude Oil': 100,
			'Water': 50,
		},
		'machine': 'refinery'
	},

	'Low Density Structure': {
		'time': 20,
		'ingredients': {
			'Copper Plate': 20,
			'Steel Plate': 2,
			'Plastic Bar': 5
		}
	},

	'Rocket Control Unit': {
		'time': 30,
		'ingredients': {
			'Processing Unit': 1,
			'Speed Module': 1
		}
	},

	'Speed Module': {
		'time': 15,
		'ingredients': {
			'Electronic Circuit': 5,
			'Advanced Circuit': 5
		},
		'prod_module': False
	},

	'Rocket Fuel': {
		'time': 30,
		'ingredients': {
			'Solid Fuel': 10,
			'Light Oil (Naive)': 10
		}
	},

	'Solid Fuel': {
		'time': 2,
		'ingredients': {
			'Light Oil (Naive)': 10
		},
		'machine': 'chemplant'
	},

	'Automation Science Pack': {
		'time': 5,
		'ingredients': {
			'Copper Plate': 1,
			'Iron Gear Wheel': 1
		}
	},

	'Production Science Pack': {
		'time': 21,
		'output': 3,
		'ingredients': {
			'Rail': 30,
			'Electric Furnace': 1,
			'Productivity Module': 1
		}
	},

	'Rail': {
		'time': 0.5,
		'output': 2,
		'ingredients': {
			'Stone': 1,
			'Steel Plate': 1,
			'Iron Stick': 1
		},
		'prod_module': False
	},

	'Iron Stick': {
		'time': 0.5,
		'output': 2,
		'ingredients': {
			'Iron Plate': 1
		}
	},

	'Electric Furnace': {
		'time': 5,
		'ingredients': {
			'Steel Plate': 10,
			'Advanced Circuit': 5,
			'Stone Brick': 10
		},
		'prod_module': False
	},

	'Stone Brick': {
		'time': 3.2,
		'ingredients': {
			'Stone': 2
		},
		'machine': 'furnace'
	},

	'Productivity Module': {
		'time': 15,
		'ingredients': {
			'Electronic Circuit': 5,
			'Advanced Circuit': 15
		},
		'prod_module': False
	},

	'Laser Turret': {
		'time': 45,
		'ingredients': {
			'Steel Plate': 20,
			'Battery': 12,
			'Electronic Circuit': 20
		},
		'prod_module': False
	},

	'Military Science Pack': {
		'time': 10,
		'output': 2,
		'ingredients': {
			'Piercing Rounds Magazine': 1,
			'Grenade': 1,
			'Wall': 2
		}
	},

	'Grenade': {
		'time': 8,
		'ingredients': {
			'Coal': 10,
			'Iron Plate': 5
		},
		'prod_module': False
	},

	'Wall': {
		'time': 0.5,
		'ingredients': {
			'Stone Brick': 5
		},
		'prod_module': False
	},

	'Chemical Science Pack': {
		'time': 24,
		'output': 2,
		'ingredients': {
			'Sulfur': 1,
			'Advanced Circuit': 3,
			'Engine Unit': 2
		}
	}
}
