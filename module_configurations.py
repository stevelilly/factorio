machines = {
	'Asm3': {
		'type': 'assembler',
		'energy': 375,
		'speed': 1.25,
		'pollution': 2,
		'slots': 4
	},

	'Electric Mining Drill': {
		'type': 'miner',
		'energy': 90,
		'pollution': 10,
		'speed': 0.5,
		'slots': 3
	},

	'Electric Furnace': {
		'type': 'furnace',
		'energy': 180,
		'pollution': 5,
		'speed': 2,
		'slots': 2
	},

	'Chemical Plant': {
		'type': 'chemplant',
		'energy': 210,
		'pollution': 4,
		'speed': 1,
		'slots': 3
	},

	'Oil Refinery': {
		'type': 'refinery',
		'energy': 420,
		'pollution': 6,
		'speed': 1,
		'slots': 3
	},

	'Pumpjack': {
		'type': 'pumpjack',
		'energy': 90,
		'pollution': 10,
		'speed': 1,
		'slots': 2
	},

	'Pump': {
		'type': 'pump',
		'energy': 0,
		'pollution': 0,
		'speed': 12000,
		'slots': 0
	}
}

modules = {
	'Eff1': {
		'energy': -30
	},

	'Eff2': {
		'energy': -40
	},

	'Speed1': {
		'energy': +50,
		'speed': +20
	},

	'Speed2': {
		'energy': +60,
		'speed': +30
	},

	'Speed3': {
		'energy': +70,
		'speed': +50		
	},

	'Prod1': {
		'energy': +40,
		'speed': -15,
		'productivity': +4,
		'pollution': +10
	},

	'Prod2': {
		'energy': +60,
		'speed': -15,
		'productivity': +6,
		'pollution': +7	
	},

	'Prod3': {
		'energy': +80,
		'speed': -15,
		'productivity': +10,
		'pollution': +10
	}
}


def all_combinations(options, num_chosen):
	def _calc(num_chosen, choice_index):
		if num_chosen == 0:
			yield []
		else:
			for i in range(choice_index, len(options)):
				head = [options[i]]
				for tail in _calc(num_chosen - 1, i):
					yield head + list(tail)
	return _calc(num_chosen, 0)


def determine_properties_with_modules(module_names):
	properties = {
		'energy': 100,
		'speed': 100,
		'productivity': 100,
		'pollution': 100
	}
	for module_name in module_names:
		for name, value in modules[module_name].items():
			properties[name] += value
	# minimum energy consumption is 20%
	if properties['energy'] < 20:
		properties['energy'] = 20
	return properties


machine_types = set()
for machine_info in machines.values():
	machine_types.add(machine_info['type'])
print("Machine types: %s" % machine_types)


module_configurations = {}
for machine_type in machine_types:
	configurations = []
	for machine_name, machine_info in machines.items():
		if machine_info['type'] != machine_type:
			continue
		for slots_filled in range(0, machine_info['slots'] + 1):
			for configuration in all_combinations(list(modules.keys()), slots_filled):
				module_properties = determine_properties_with_modules(configuration)

				properties = {
					'name': ",".join([machine_name] + configuration),
					'speed': machine_info['speed'] * module_properties['speed'] * 0.01,
					'energy': machine_info['energy'] * module_properties['energy'] * 0.01,
					'pollution': machine_info['pollution'] * module_properties['pollution'] * 0.01 * module_properties['energy'] * 0.01,
					'productivity': module_properties['productivity'] * 0.01
				}
				configurations.append(properties)
	module_configurations[machine_type] = configurations
